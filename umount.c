#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/fsuid.h>
#include <sys/mount.h>

#define MOUNT_STATE "/proc/mounts"

int main(int argc, char* argv[]) {
    FILE* mounts = fopen(MOUNT_STATE, "r");
    char line[256];

    while (fgets(line, sizeof(line), mounts)) {
        int i = 0;
        char *ptr = strtok(line, " ");
        char *path = NULL;
        char *options = NULL;

        while(ptr != NULL) {
            switch (i++) {
            case 0:
                if (strcmp(ptr, "xrdp-chansrv") != 0)
                    goto out;
                break;
            case 1:
                if (strcmp(ptr, argv[1]) != 0)
                    goto out;
                path = ptr;
                break;
            case 3:
                options = ptr;
                break;
            }
            ptr = strtok(NULL, " ");
        }

        ptr = strtok(options, ",");
        while(ptr != NULL) {
            char *sep = strchr(ptr, '=');

            if (sep) {
                *sep = '\0';
                if (strcmp(ptr, "user_id") == 0) {
                    int user_id = atoi(sep + 1);
                    
                    setfsuid(user_id);
                    umount2(path, 0);
                }
            }

            ptr = strtok(NULL, ",");
        }
out:
        ;
    }

    fclose(mounts);
    return 0;
}
